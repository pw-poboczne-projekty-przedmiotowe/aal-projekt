#include "Sort.hpp"
#include <list>

Sort::Sort(vector<char> v)
{
    std::cout << "Chary";
    entryString = v;
    sortedStringA = v;
    sortedStringB = v;
    sortedStringC = v;
}

Sort::Sort(string s)
{
    for (int i = 0; i < s.size(); i++)
    {
        entryString.push_back(s[i]);
    }
    sortedStringA = entryString;
    sortedStringB = entryString;
    sortedStringC = entryString;
    sortedStringBrutal = entryString;
    entryInString = s;

    steps = 0;

}

Sort::Sort()
{
}

Sort::~Sort()
{
}

void Sort::moveToEnd(int x, vector<char> &array)
{
    array.insert(array.end(), array.begin() + x, array.begin() + x + 4);
    array.erase(array.begin() + x, array.begin() + x + 4);

    steps++;
}

bool Sort::checkContainersSorted(std::vector<char> &array)
{
    bool isSorted = true;
    char tmp;
    char prev;

    for(int i = 1; i < array.size(); i++)
    {
        char tmp = array[i]; 
        char prev = array[i-1];

        if((tmp == 'C' && (prev == 'M' || prev == 'Y' || prev == 'K')) || (tmp == 'M' && (prev == 'Y' || prev == 'K')) || (tmp == 'Y' && prev == 'K'))
            isSorted = false;

    }

    return isSorted;
}

void Sort::brutal(std::vector<char> &array)
{
    const int LIMIT = 3000000;
    std::list<std::vector<char>> possibilities;
    possibilities.push_back(array);
    auto tmp = array;
    int counter = 0;
    int next_step_number = 0;
    int moves = 0;
    int lastMoveIndex = array.size() - 4;
    while (1)
    {
        tmp = possibilities.front();
        if (checkContainersSorted(tmp))
            break;

        if (counter == next_step_number)
        {
            moves++;
            next_step_number += next_step_number * (lastMoveIndex + 1);
        }
        for (int a = 0; a < lastMoveIndex; a++)
        {
            auto tmp2 = tmp;
            moveToEnd(a, tmp2);
            possibilities.push_back(tmp2);
            if (possibilities.size() >= LIMIT)
            {
                steps = -1;
                return;
            }
        }
        counter++;
        possibilities.pop_front();
    }
    array = tmp;
    steps = moves;
}

int Sort::findNextContainerNamed(char c, int pos, std::vector<char> &array)
{
    while (array[pos] != c)
        pos++;

    return pos;
}

int Sort::findNextContainerNotNamed(char c, int pos, std::vector<char> &array)
{
    while (array[pos] == c)
        pos++;

    return pos;
}

void Sort::sortOneContainerBasic(int start, int count, char c, std::vector<char> &array)
{
    int tmp;
    int tmpLenght = array.size() - start;

    if (tmpLenght >= 5)
    {
        if ((count - start) % 4 == 0)
        {
            for (int i = 0; i < ((count - start) / 4); i++)
                moveToEnd(start, array);
        }
        else
        {
            tmp = tmpLenght % 4;

            switch (tmp)
            {
            case 0:
                if (count < array.size() - 3)
                {
                    if(count < array.size() - 4 )
                        moveToEnd(count, array);

                    for (int i = 0; i < (tmpLenght / 4 - 1); i++)
                        moveToEnd(start, array);
                }
                else
                {
                    switch (array.size() - (count + 1))
                    {      
                    case 0: // ostatnie miejsce

                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);

                        for (int i = 0; i < (tmpLenght / 4 - 1); i++)
                            moveToEnd(start, array);

                        break;
                 
                    case 1: // przedostatnie miejsce

                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);

                        for (int i = 0; i < (tmpLenght / 4 - 1); i++)
                            moveToEnd(start, array);

                        break;

                    case 2: // przed przedostatnie miejsce

                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);

                        for (int i = 0; i < (tmpLenght / 4 - 1); i++)
                            moveToEnd(start, array);

                        break;
                    
                    default:
                        break;
                    }
                }
                break;

            case 1:
                if (count < array.size() - 3)
                {
                    if(count < array.size() - 4 )
                        moveToEnd(count, array);
                    
                    moveToEnd(array.size() - 5, array);
                    moveToEnd(array.size() - 5, array);
                    moveToEnd(array.size() - 5, array);

                    for (int i = 0; i < (tmpLenght / 4); i++)
                        moveToEnd(start, array);
                }
                else
                {
                    switch (array.size() - (count + 1))
                    {      
                    case 0: // ostatnie miejsce

                        for (int i = 0; i < (tmpLenght / 4); i++)
                            moveToEnd(start, array);

                        break;
                 
                    case 1: // przedostatnie miejsce

                        moveToEnd(array.size() - 5, array);

                        for (int i = 0; i < (tmpLenght / 4); i++)
                            moveToEnd(start, array);

                        break;

                    case 2: // przed przedostatnie miejsce

                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);

                        for (int i = 0; i < (tmpLenght / 4); i++)
                            moveToEnd(start, array);

                        break;
                    
                    default:
                        break;
                    }
                }
                break;

            case 2:
                if (count < array.size() - 3)
                {
                    if(count < array.size() - 4 )
                        moveToEnd(count, array);
                    
                    moveToEnd(array.size() - 5, array);
                    moveToEnd(array.size() - 5, array);

                    for (int i = 0; i < (tmpLenght / 4); i++)
                        moveToEnd(start, array);
                }
                else
                {
                    switch (array.size() - (count + 1))
                    {      
                    case 0: // ostatnie miejsce

                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);

                        for (int i = 0; i < (tmpLenght / 4); i++)
                            moveToEnd(start, array);

                        break;
                 
                    case 1: // przedostatnie miejsce

                        for (int i = 0; i < (tmpLenght / 4); i++)
                            moveToEnd(start, array);

                        break;

                    case 2: // przed przedostatnie miejsce

                        moveToEnd(array.size() - 5, array);

                        for (int i = 0; i < (tmpLenght / 4); i++)
                            moveToEnd(start, array);

                        break;
                    
                    default:
                        break;
                    }
                }
                break;

            case 3:
                if (count < array.size() - 3)
                {
                    if(count < array.size() - 4 )
                        moveToEnd(count, array);
                    
                    moveToEnd(array.size() - 5, array);

                    for (int i = 0; i < (tmpLenght / 4); i++)
                        moveToEnd(start, array);
                }
                else
                {
                    switch (array.size() - (count + 1))
                    {      
                    case 0: // ostatnie miejsce

                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);

                        for (int i = 0; i < (tmpLenght / 4); i++)
                            moveToEnd(start, array);

                        break;
                 
                    case 1: // przedostatnie miejsce

                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);
                        moveToEnd(array.size() - 5, array);

                        for (int i = 0; i < (tmpLenght / 4); i++)
                            moveToEnd(start, array);

                        break;

                    case 2: // przed przedostatnie miejsce

                        for (int i = 0; i < (tmpLenght / 4); i++)
                            moveToEnd(start, array);

                        break;
                    
                    default:
                        break;
                    }
                }
                break;

            default:
                break;
            }
        }
    }
}

void Sort::sortOneContainerBasicImproved(int start, int count, char c, std::vector<char> &array)
{
    int tmp;
    int tmpLenght = array.size() - start;
    if (tmpLenght > 6)
    {
        if ((count - start) % 4 == 0)
        {
            for (int i = 0; i < ((count - start) / 4); i++)
                moveToEnd(start, array);
        }
        else
        {
            tmp = tmpLenght % 4;

            switch (tmp)
            {
            case 0:
                if (count >= array.size() - 3)
                {
                    moveToEnd(start, array);
                    count -= 4;
                }
                moveToEnd(count, array);

                for (int i = 0; i < (tmpLenght / 4 - 1); i++)
                    moveToEnd(start, array);

                break;

            case 1:

                if (count - start >= 3)
                {
                    moveToEnd(count - 3, array);
                }
                else
                {
                    moveToEnd(count, array);
                    moveToEnd(array.size() - 5, array);
                    moveToEnd(array.size() - 5, array);
                    moveToEnd(array.size() - 5, array);
                }

                for (int i = 0; i < (tmpLenght / 4); i++)
                    moveToEnd(start, array);

                break;

            case 2:
                if (count - start >= 2 && count < array.size() - 1)
                {
                    moveToEnd(count - 2, array);
                }
                else if (count == array.size() - 1)
                {
                    moveToEnd(array.size() - 5, array);
                    count -= 4;
                    moveToEnd(count - 2, array);
                }
                else if (count - start < 2)
                {
                    moveToEnd(count, array);
                    moveToEnd(array.size() - 6, array);
                }

                for (int i = 0; i < (tmpLenght / 4); i++)
                    moveToEnd(start, array);
                break;

            case 3:
                if (count - start >= 1 && count < array.size() - 3)
                {
                    moveToEnd(count - 1, array);

                    for (int i = 0; i < (tmpLenght / 4); i++)
                        moveToEnd(start, array);
                }
                else if (count >= array.size() - 2)
                {
                    moveToEnd(count - 4, array);
                    count -= 4;
                    moveToEnd(count - 1, array);

                    for (int i = 0; i < (tmpLenght / 4); i++)
                        moveToEnd(start, array);
                }
                break;

            default:
                break;
            }
        }
    }
    else if (tmpLenght == 6)
    {
        switch (count - start)
        {
        case 0:
            break;
        case 1:
            moveToEnd(start, array);
            moveToEnd(array.size() - 5, array);
            moveToEnd(start, array);
            break;
        case 2:
            moveToEnd(start, array);
            moveToEnd(start, array);
            break;
        case 3:
            moveToEnd(start + 1, array);
            moveToEnd(start, array);
            break;
        case 4:
            moveToEnd(start, array);
            break;
        case 5:
            moveToEnd(start, array);
            moveToEnd(start, array);
            moveToEnd(array.size() - 5, array);
            moveToEnd(start, array);
            break;
        default:
            break;
        }
    }
    else
    {
        switch (count - start)
        {
        case 0:
            break;
        case 1:
            moveToEnd(start, array);
            moveToEnd(start, array);
            moveToEnd(start, array);
            moveToEnd(start, array);
            break;
        case 2:
            moveToEnd(start, array);
            moveToEnd(start, array);
            moveToEnd(start, array);
            break;
        case 3:
            moveToEnd(start, array);
            moveToEnd(start, array);
            break;
        case 4:
            moveToEnd(start, array);
            break;
        default:
            break;
        }
    }
}

void Sort::setToBeginning(int start, int amountOfContainersToSort, char c, std::vector<char> &array)
{
    if (amountOfContainersToSort < 7)
    {
        for (int i = start; i < start + amountOfContainersToSort; i++)
            sortOneContainerBasicImproved(i, findNextContainerNamed(c, i, array), c, array);
    }
    else
    {
        int li = array.size() - start - amountOfContainersToSort;
        if (li % 4 > 0)
        {
            sortOneContainerBasicImproved(start, findNextContainerNamed(c, start, array), c, array);

            if (li % 4 == 1)
            {
                sortOneContainerBasicImproved(start + 1, findNextContainerNamed(c, start + 1, array), c, array);
                sortOneContainerBasicImproved(start + 2, findNextContainerNamed(c, start + 2, array), c, array);
            }
            else if (li % 4 == 2)
            {
                sortOneContainerBasicImproved(start + 1, findNextContainerNamed(c, start + 1, array), c, array);
                sortOneContainerBasicImproved(start + 2, findNextContainerNotNamed(c, start + 2, array), c, array);
            }
            else if (li % 4 == 3)
            {
                sortOneContainerBasicImproved(start + 1, findNextContainerNotNamed(c, start + 1, array), c, array);
                sortOneContainerBasicImproved(start + 2, findNextContainerNotNamed(c, start + 2, array), c, array);
            }

            sortOneContainerBasicImproved(start + 3, findNextContainerNotNamed(c, start + 3, array), c, array);

            sortOneContainerBasicImproved(start + 4, findNextContainerNamed(c, start + 4, array), c, array);
            sortOneContainerBasicImproved(start + 5, findNextContainerNamed(c, start + 5, array), c, array);
            sortOneContainerBasicImproved(start + 6, findNextContainerNamed(c, start + 6, array), c, array);
        }
        else
        {
            sortOneContainerBasicImproved(start, findNextContainerNamed(c, start, array), c, array);
            sortOneContainerBasicImproved(start + 1, findNextContainerNamed(c, start + 1, array), c, array);
            sortOneContainerBasicImproved(start + 2, findNextContainerNamed(c, start + 2, array), c, array);
        }

        int head = array.size();
        int tail = array.size();
        int lwtr = li / 4;
        int ltr = 0;
        int lio = 0;

        while (ltr < lwtr)
        {
            if (lio < 4)
            {
                int LiczI = 1;
                do
                {
                    head--;
                } while (array[head] == c);
                head--;
                if (array[head] != c)
                {
                    LiczI++;
                }
                head--;
                if (array[head] != c)
                {
                    LiczI++;
                }
                head--;
                if (array[head] != c)
                {
                    LiczI++;
                }
                if (LiczI < 4)
                {
                    moveToEnd(head, array);
                    tail -= 4;
                    lio += LiczI;
                }
                else
                {
                    ltr++;
                }
            }
            else
            {
                int pocz = tail;
                for (pocz; pocz <= tail + 3; pocz++)
                {
                    sortOneContainerBasicImproved(pocz, findNextContainerNotNamed(c, pocz, array), c, array);
                }

                tail += 4;
                ltr++;
                lio -= 4;
                if (lio == 0)
                    tail = array.size();
                else
                {
                    while (array[tail] == c)
                        tail++;
                    if (!(tail < array.size() - 4))
                    {
                        tail = array.size() - 4;
                    }
                }
            }
        }

        if (li % 4 > 0)
        {
            moveToEnd(start, array);
        }
            
        int pocz = start;
        for (int k = 0; k < lwtr; k++)
        {
            while (array[pocz] == c)
                pocz++;
            moveToEnd(pocz, array);
        }
    }
}

void Sort::algorithmBrutal()
{
    brutal(sortedStringBrutal);
}

void Sort::algorithmA()
{
    int start = 0;
    int count = 0;
    int tmpCount = 0;
    char nowSortingLetter = 'C';

    int countC = 0;
    int countM = 0;
    int countY = 0;

    for (auto it : sortedStringA)
    {
        switch (it)
        {
        case 'C':
            countC++;
            break;

        case 'M':
            countM++;
            break;

        case 'Y':
            countY++;
            break;

        default:
            break;
        }
    }

    for (int i = start; i < countC; i++)
            sortOneContainerBasicImproved(i, findNextContainerNamed(nowSortingLetter, i, sortedStringA), nowSortingLetter, sortedStringA);

    nowSortingLetter = 'M';
    for (int i = countC; i < countC + countM; i++)
            sortOneContainerBasicImproved(i, findNextContainerNamed(nowSortingLetter, i, sortedStringA), nowSortingLetter, sortedStringA);
    
    nowSortingLetter = 'Y';
    for (int i = countC + countM; i < countC + countM + countY; i++)
            sortOneContainerBasicImproved(i, findNextContainerNamed(nowSortingLetter, i, sortedStringA), nowSortingLetter, sortedStringA);

}
void Sort::algorithmB()
{
    int countC = 0;
    int countM = 0;
    int countY = 0;
    int countK = 0;
    int start = 0;

    for (auto it : sortedStringB)
    {

        switch (it)
        {
        case 'C':
            countC++;
            break;
        case 'M':
            countM++;
            break;
        case 'Y':
            countY++;
            break;
        case 'K':
            countK++;
            break;
        }
    }

    setToBeginning(start, countC, 'C', sortedStringB);
    setToBeginning(start + countC, countM, 'M', sortedStringB);
    setToBeginning(start + countC + countM, countY, 'Y', sortedStringB);

}
void Sort::algorithmC()
{
    int start = 0;
    int count = 0;
    int tmpCount = 0;
    char nowSortingLetter = 'C';

    int countC = 0;
    int countM = 0;
    int countY = 0;

    for (auto it : sortedStringC)
    {
        switch (it)
        {
        case 'C':
            countC++;
            break;

        case 'M':
            countM++;
            break;

        case 'Y':
            countY++;
            break;

        default:
            break;
        }
    }

    for (int i = start; i < countC; i++)
            sortOneContainerBasic(i, findNextContainerNamed(nowSortingLetter, i, sortedStringC), nowSortingLetter, sortedStringC);

    nowSortingLetter = 'M';
    for (int i = countC; i < countC + countM; i++)
            sortOneContainerBasic(i, findNextContainerNamed(nowSortingLetter, i, sortedStringC), nowSortingLetter, sortedStringC);
    
    nowSortingLetter = 'Y';
    for (int i = countC + countM; i < countC + countM + countY; i++)
            sortOneContainerBasic(i, findNextContainerNamed(nowSortingLetter, i, sortedStringC), nowSortingLetter, sortedStringC);
}

void Sort::setEntryString(vector<char> a)
{
    entryString = a;
}
vector<char> Sort::getEntryString()
{
    return entryString;
}

vector<char> Sort::getSortedStringA()
{
    return sortedStringA;
}
vector<char> Sort::getSortedStringB()
{
    return sortedStringB;
}
vector<char> Sort::getSortedStringC()
{
    return sortedStringC;
}

vector<char> Sort::getSortedStringBrutal()
{
    return sortedStringBrutal;
}

void Sort::showA()
{
    for (auto it : sortedStringA)
    {
        std::cout << it << " ";
    }
}

void Sort::showB()
{
    for (auto it : sortedStringB)
    {
        std::cout << it << " ";
    }
}

void Sort::showC()
{
    for (auto it : sortedStringC)
    {
        std::cout << it << " ";
    }
}

void Sort::showBrutal()
{
    for (auto it : sortedStringBrutal)
    {
        std::cout << it << " ";
    }
}