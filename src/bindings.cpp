#include <boost/python.hpp>
#include <boost/python/raw_function.hpp>

#include "hello_ext.hpp"
#include "Sort.hpp"
#include "Timer.hpp"


namespace python = boost::python;

template <class T> struct VecToList
{
    static PyObject *convert(const std::vector<T> &vec)
    {
        boost::python::list *l = new boost::python::list();
        for (auto &&i : vec)
        {
            l->append(i);
        }
        return l->ptr();
    }
};


//Boost.Python wrapper defining the module name, classes  and methods
BOOST_PYTHON_MODULE(bindings){
    python::class_<Hello>("Hello")//TODO remove it
        //.def(init<double, double>()) //another constructor if defined
        .def("greet",&Hello::greet)
        .def("set",&Hello::set)
        //.def_readonly("len", &Hello::len)
        .def_readwrite("len", &Hello::len)
        ;
    python::def("greeter", greeter);
    python::class_<Sort>("Sort",python::no_init)
        .def(python::init<string>())
        .def(python::init<vector<char>>())
        .def("algA", &Sort::algorithmA)
        .def("algB", &Sort::algorithmB)
        .def("algC", &Sort::algorithmC)
        .def("algBrutal",&Sort::algorithmBrutal)
        .def("getResultA", &Sort::getSortedStringA)
        .def("getResultB", &Sort::getSortedStringB)
        .def("getResultC", &Sort::getSortedStringC)
        .def("getResultBrutal",&Sort::getSortedStringBrutal)
        .def("getSteps", &Sort::getSteps)
        ;
    python::class_<Timer>("Timer", python::no_init)
        .def(python::init<Sort>())
        .def("measureAlg", &Timer::measureAlg)
        .def("getSec", &Timer::getSeconds)
        .def("getMilisec", &Timer::getMiliseconds)
        .add_property("sort", &Timer::sort)
        ;
    python::to_python_converter<std::vector<char, std::allocator<char>>, VecToList<char>>();
}