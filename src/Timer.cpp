#include "Timer.hpp"

Timer::Timer(Sort s)
    : sort(s)
{
}

void Timer::measureAlg(int alg_nr)
{
    std::chrono::_V2::system_clock::time_point start;
    std::chrono::_V2::system_clock::time_point end;
    switch (alg_nr)
    {
    case 1:
        start = high_resolution_clock::now();
        sort.algorithmA();
        end = high_resolution_clock::now();
        break;
    case 2:
        start = high_resolution_clock::now();
        sort.algorithmB();
        end = high_resolution_clock::now();
        break;
    case 3:
        start = high_resolution_clock::now();
        sort.algorithmC();
        end = high_resolution_clock::now();
        break;
    case 4:
        start = high_resolution_clock::now();
        sort.algorithmBrutal();
        end = high_resolution_clock::now();
        break;

    default:
        const char msg[] = "Zla wartosc testu, jedyne wartosci to 1 dla A 2 dla B i 3 dla C i 4 dla brutalnego";
        std::cout << msg;
        throw std::out_of_range(msg);
        break;
    }
    time_ = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
}

void Timer::measureFuncTime(function<void()> &fun)
{
    auto start = high_resolution_clock::now();
    fun();
    auto end = high_resolution_clock::now();
    time_ = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
}

Timer::~Timer()
{
}