#!/usr/bin/python3

import argparse
import fileinput
import os
import random
import sys
import math

import matplotlib.pyplot as plt

module_path = os.path.dirname(os.path.realpath(__file__)) + "/build/"
sys.path.append(module_path)

try:
    from bindings import *
except:
    print("Something went wrong during bindings import")
    print("__file__ : " + __file__)
    print("sys.path: " + str(sys.path))


def l_to_str(list_of_chars):
    s = ""
    for c in list_of_chars:
        s += c
    return s


def generate_random_strings(length: int, number_of_strings: int = 1, chance_of_the_next_equal: int = 0.25, attempt=0) -> list:
    result = []
    for num1 in range(number_of_strings):
        string = random.choice(["C", "M", "Y", "K"])
        if length <= 4:
            string = "KKKK"
        else:
            for num2 in range(length - 1):
                chance = random.randint(0, 1)
                if chance <= chance_of_the_next_equal:
                    selected_character = string[-1]
                else:
                    other_colors = list(
                        {"C", "M", "Y", "K"}.difference(string[-1]))
                    selected_character = random.choice(other_colors)
                string += selected_character
        if string.count("K") < 4:
            if attempt > 100:
                raise TimeoutError(
                    "Unable to generate string during 100 attempts for len :"+str(length))
            string = generate_random_strings(
                length, 1, chance_of_the_next_equal, attempt + 1)[0]
        result.append(string)
    return result


def check_sorting(sequence: str) -> bool:
    chars = {0: "C", 1: "M", 2: "Y", 3: "K"}
    current_char = 0
    for num in range(len(sequence)):
        if sequence[num] != chars[current_char]:
            if current_char == 3:
                return False
            if sequence[num] == chars[current_char+1]:
                current_char += 1
            else:
                return False
    return True


def calculate_theoretical(alg_nr, size):
    if alg_nr == 2:
        return size
    elif alg_nr == 3 or alg_nr == 1:
        return size*size
    elif alg_nr == 4:
        return math.pow(size, math.log(size))
    raise NotImplementedError


if __name__ == "__main__":
    alg_versions = {"a": 1, "b": 2, "c": 3, "brutal": 4}

    parser = argparse.ArgumentParser(
        description="Script used for testing algorithms for sorting CMYK", epilog="d")

    parser.add_argument("mode", choices=["pipe", "autogen", "test", "cmd"],
                        help="Mode of operation. "+"pipe - input data is put in pipe"
                        + "autogen - use autogenerated data" + " test - TODO"
                        + "cmd - sequence will be passed via argument")
    parser.add_argument(
        "alg", choices=alg_versions.keys(), help="ID of used algorithm")

    parser.add_argument("--length", "-l", type=int, default=60,
                        help="Length of autogenerated sequences (only for autogen) in case of test it is max size")
    parser.add_argument("--number", "-n", type=int, default=1,
                        help="Number of autogenerated sequences (only for autogen and test)")
    parser.add_argument("--sequence", type=str,
                        help="Provided single sequence manually")
    parser.add_argument("--graph", "-g", action="store_true",
                        help="Draw graph (only test")
    parser.add_argument("--step", "-s", type=int, default=40,
                        help="How big is step (only test)")

    args = parser.parse_args()

    unsorted_sequences = []
    if args.mode == "pipe":
        for line in sys.stdin:
            unsorted_sequences.append(line[0:-1])  # we have to get rid of endl

    elif args.mode == "autogen":
        unsorted_sequences = generate_random_strings(args.length, args.number)

    elif args.mode == "cmd":
        if args.sequence is None:
            raise ValueError
        unsorted_sequences = [args.sequence]

    print(unsorted_sequences)
    if args.mode == "autogen" or args.mode == "pipe" or args.mode == "cmd":
        for seq in unsorted_sequences:
            s = Sort(seq)
            result = ""
            if args.alg == "a":
                s.algA()
                result = s.getResultA()
            if args.alg == "b":
                s.algB()
                result = s.getResultB()
            if args.alg == "c":
                s.algC()
                result = s.getResultC()
            if args.alg == "brutal":
                s.algBrutal()
                result = s.getResultBrutal()

            if not check_sorting(result):
                print("❗Sequence: \n"+l_to_str(result))
                print("Based on: \n" + seq + "\nIs not sorted correctly")
                sys.exit()

            sys.stdout.write(l_to_str(result) + "\n")

    if args.mode == "test":
        sizes = []
        times_avg = []
        theoretical = []

        alg_nr = alg_versions[args.alg]
        for number in range(args.step, args.length, args.step):
            unsorted_sequences = generate_random_strings(number, args.number)
            times = []
            for seq in unsorted_sequences:
                s = Sort(seq)
                t = Timer(s)
                t.measureAlg(alg_nr)
                times.append(t.getMilisec())

            sizes.append(number)
            average_time = sum(times)/len(times)
            times_avg.append(average_time)
            estimation = calculate_theoretical(alg_nr, number)
            theoretical.append(estimation)
            print("size: " + str(number) + " time(ms): " +
                  str(round(average_time, 2)) + " estimated value: " + str(estimation))

        if args.graph:
            fig, host = plt.subplots()

            plt.title("Measurements for algorithm: " + args.alg)
            par1 = host.twinx()
            host.set_xlabel("size")
            host.set_ylabel("time (ms)")
            par1.set_ylabel("estimated time")

            p1, = host.plot(sizes, times_avg, "b-")
            p2, = par1.plot(sizes, theoretical, "r-")

            host.set_ylim(0, times_avg[-1]*1.1)
            par1.set_ylim(0, theoretical[-1]*1.1)

            tkw = dict(size=4, width=1.5)
            host.tick_params(axis='y', colors=p1.get_color(), **tkw)
            par1.tick_params(axis='y', colors=p2.get_color(), **tkw)
            host.tick_params(axis='x', **tkw)

            plt.show()
