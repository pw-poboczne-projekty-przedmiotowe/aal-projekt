# AAL - projekt



## Użycie prekompilowanych bibliotek z C++

**Budowanie C++**
```bash
mkdir build; cd build
cmake ..
make
```
**Użycie**
```bash
python3
```
Gdy python jest otwierany z folderu  `build`.
```python
from hello_ext import greeter
```
Przy otwarciu w innej lokacji
```python
__file__ = "./load_libs.py"
exec(open(__file__).read())
```