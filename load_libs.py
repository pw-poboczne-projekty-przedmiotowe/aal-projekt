#!/usr/bin/python3 -i

import os
import sys

module_path = os.path.dirname(os.path.realpath(__file__)) + "/build/"
sys.path.append(module_path)

try:
    from bindings import *
    print("Library imported")
except:
    print("Something went wrong during import")
    print("__file__ : " + __file__)
    print("sys.path: " + str(sys.path))