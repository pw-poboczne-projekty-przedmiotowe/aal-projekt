#pragma once
#include <chrono>
#include <exception>
#include <functional>
#include <iostream>

#include "Sort.hpp"

using std::function;
using namespace std::chrono;

class Timer
{
private:
    microseconds time_;

public:
    Sort sort;

    Timer(Sort s);
    ~Timer();

    void measureAlg(int alg_nr);
    void measureFuncTime(function<void()> &fun);
    float getMiliseconds() { return time_.count() / 1000.0; };
    float getSeconds() { return time_.count() / 1000000.0; };
};