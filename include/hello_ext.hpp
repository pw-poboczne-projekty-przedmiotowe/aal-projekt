
//Class/Struct to be exposed to python
class Hello{
private:
    std::string msg;
public:
    //Hello(std::string msg): msg(msg) {} //overloaded constructor

    std::string greet(){
        //std::cout << "Hello World!\n";
        return msg;
    }   

    //Set the message string
    void set(std::string text){
        this->msg = text;
    }   
    int len; //public data member
};

//Raw cpp function
char const* greeter(){
   return "hello World! from outside class";
}

