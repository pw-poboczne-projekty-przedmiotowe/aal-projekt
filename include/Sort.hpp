#pragma once

#include <vector>
#include <string>
#include <iostream>

using namespace std;
class Sort {

private:

    string entryInString;

    int steps;

    vector<char> entryString;

    vector<char> sortedStringA;
    vector<char> sortedStringB;
    vector<char> sortedStringC;   
    vector<char> sortedStringBrutal;

    void moveToEnd(int x, vector<char> &array);
    int findNextContainerNamed(char c, int pos, std::vector<char> &array);
    int findNextContainerNotNamed(char c, int pos, std::vector<char> &array);

    void sortOneContainerBasic(int start, int count, char c, std::vector<char> &array);
    void sortOneContainerBasicImproved(int start, int count, char c, std::vector<char> &array);

    void setToBeginning(int start, int amountOfContainersToSort, char c, std::vector<char> &array);

    void brutal(std::vector<char> &array);

    bool checkContainersSorted(std::vector<char> &array);

public:
    Sort(vector<char> v);
    Sort(string s);
    Sort();
    ~Sort();

    void algorithmA();
    void algorithmB();
    void algorithmC();
    void algorithmBrutal();

    void setEntryString(vector<char> a);
    vector<char> getEntryString();

    vector<char> getSortedStringA();
    vector<char> getSortedStringB();
    vector<char> getSortedStringC();
    vector<char> getSortedStringBrutal();

    int getSteps() { return steps; };

    void showA();
    void showB();
    void showC();
    void showBrutal();

};
